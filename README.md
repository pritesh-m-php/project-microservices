# MicroServices Application

## Overview

This repository contains a microservices-based application with three main components:

1. User Microservice (`user_microservice`)
2. Product Microservice (`product_microservice`)
3. API Gateway (`api_gateway`)

These microservices are designed to work together to provide a complete application. This readme will guide you through the system requirements and how to start the application.

## System Requirements

Before you begin, ensure that your system meets the following requirements:

- **PHP 8.2**: This application requires PHP version 8.2 or later to be installed on your system.

- **Xdebug PHP Extension**: The Xdebug extension is required for code debugging and profiling. You can install Xdebug by following the official installation guide: [Xdebug Installation Guide](https://xdebug.org/docs/install).
- My first suggestion is to check your Xdebug configuration in the php.ini file
    ```bash
        zend_extension="xdebug.so"
        xdebug.mode=develop,debug,coverage
        xdebug.start_with_request = yes
     ```
- **Composer V2**: Make sure you have Composer version 2.x installed on your system. You can install or update Composer to the latest version by following the official documentation: [Composer Installation](https://getcomposer.org/doc/00-intro.md#installation).
- **MySQL**: Make sure you have MySQL server installed in your system.
- **Apache Server**: Make sure you have Apache server installed in your system.

## Getting Started

To start the application, follow these steps:

1. **Clone the Repository**: Start by cloning this repository to your local machine:

   ```bash
   git clone https://github.com/your-username/project-microservices.git
   ```

2. **Database Configuration in Both Microservice**: Rename ".env.example" to .env and configure database connection:

   ```bash
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=
   DB_USERNAME=
   DB_PASSWORD=
   ```

   Run the below artisan command to generate the database schemas and seeder for dump some data.

   ```bash
   php artisan migrate
   php artisan db:seed
   ```

3. **Services Endpoints Configuration**: default already set but you can change it at the ".env" API gateway directory. but make sure to also change at the server.sh

   ```bash
   PRODUCTS_SERVICE_BASE_URI=http://localhost:8100/api
   USERS_SERVICE_BASE_URI=http://localhost:8200/api
   ```

4. **Navigate to the Root Directory**: Change your working directory to the root of the cloned project:

   ```bash
   cd project_microservices
   ```

5. **Set Execution Permissions**: Make the `server.sh` script executable by running the following command:

   ```bash
   chmod +x server.sh
   ```

6. **Start the Application**: Run the following command to start the application server:

   ```bash
   ./server.sh start
   ```

   The `server.sh start` script is responsible for setting up the necessary environment for the microservices and starting the application.

7. **Access the Application**: Once the server is running, you can access the microservices and the API gateway at their respective endpoints.
8. **Stop the Application**: RunTo stop the running application and microservices, use the following command:
   ```bash
   ./server.sh stop
   ```

## Routes and API Information

Here is the route information for the services:

**API Endpoint:**

```bash
http://localhost:8000/api/
```

**Product Services Route List:**

- `GET /product`: Retrieve a list of all products.
- `POST /product`: Create a new product.
- `GET /product/{id}`: Retrieve information about a single product.
- `PATCH /product/{id}`: Update the details of a product.
- `DELETE /product/{id}`: Delete a product.

**User Services Route List:**

- `GET /user`: Retrieve a list of all users.
- `POST /user`: Create a new user.
- `GET /user/{id}`: Retrieve information about a single user.
- `PATCH /user/{id}`: Update the details of a user.
- `DELETE /user/{id}`: Delete a user.

**Postman Collection :**

- A Postman collection, named "api_collection.json" is available in the root directory of this project.
- You can use this collection to test the API routes mentioned above.
- To use the collection, import it into your Postman application and start testing the routes.

## Running Test Cases

To run test cases for the microservices and gateway, follow these steps:

1. **Navigate to the Microservice or Gateway Directory**: Move to the directory of the microservice you want to test.
   **For example**, if you want to test the "Product Microservice," navigate to the "product-microservice" directory:

   ```bash
   cd product-microservice
   ```

2. **Run Tests**: Use the following command to run the test cases for the microservice:

   ```bash
   composer test
   ```

   Run Test cases and generating coverage report in HTML Format inside "/test" Directory

   ```bash
   composer test --coverage-html tests/reports/coverage
   ```

3. **Repeat for Other Microservices**: If you need to test other microservices, repeat the steps above for each one.

## Exception handling and Logs

1. **Exception:** Through global exception, the handler gives you a valid readable response.
2. **Logs:** You can see all of the error logs in the "/storage/logs" directory of microservices and gateway.
