<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Product;
use Tests\TestCase;

class ProductsUnitTest extends TestCase
{
    public function testProductModel()
    {
        $faker = \Faker\Factory::create();
        $sku = $faker->isbn10();
        $name = $faker->sentence($nbWords = 4, $variableNbWords = true);
        $price = $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2500);

        $productData = [
            'sku' => $sku,
            'name' => $name,
            'price' => $price,
        ];

        $product = Product::create($productData);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($name, $product->name);
    }

    public function testValidateSkuExist()
    {
        $sku = 'WW-OO225566DD';

        $getSku = Product::where('sku', $sku)->exists();

        $this->assertFalse($getSku, "SKU '$sku' does not exist.");
    }

    public function testTotalProductsCount()
    {
        $totalProductCount = Product::count();

        switch ($totalProductCount) {
            case $totalProductCount >= 0:
                $this->assertTrue(true);
                break;
        }
    }

    public function testTotalProductsValue()
    {
        $totalProductValue = Product::sum('price');

        switch ($totalProductValue) {
            case $totalProductValue >= 0:
                $this->assertTrue(true);
                break;
        }
    }
}
