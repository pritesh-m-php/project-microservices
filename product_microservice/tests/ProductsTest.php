<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Product;
use Tests\TestCase;

class ProductsTest extends TestCase
{

    /**
     * Create product test.
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $faker = \Faker\Factory::create();
        $sku = $faker->isbn10();
        $name = $faker->sentence($nbWords = 4, $variableNbWords = true);
        $price = $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2500);

        $product = ['sku' => $sku, 'name' => $name, 'price' => $price];
        $response = $this->call('POST', '/api/product', $product);

        $this->assertEquals(200, $response->status());
    }


    /**
     * Get all products test.
     *
     * @return void
     */
    public function testGetProducts()
    {
        $response = $this->call('GET', '/api/product');

        $this->assertEquals(200, $response->status());
    }

    /**
     * Get Invalid products test.
     *
     * @return void
     */
    public function testGetProductsInvalid()
    {
        $response = $this->call('GET', '/api/product', ['sortBy' => 'price']);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Show product test.
     *
     * @return void
     */
    public function testShowProduct()
    {
        $product =  Product::first()->id;
        $response = $this->call('GET', '/api/product/' . $product);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Update product test.
     *
     * @return void
     */
    public function testUpdateProduct()
    {
        $product =  Product::first();

        $faker = \Faker\Factory::create();
        $sku = $faker->isbn10();
        $name = $faker->sentence($nbWords = 4, $variableNbWords = true);
        $price = $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2500);

        $data = ['sku' => $sku, 'name' => $name, 'price' => $price];
        $response = $this->call('PATCH', '/api/product/' . $product->id, $data);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Update user with no change test.
     *
     * @return void
     */
    public function testUpdateUserWithNoChange()
    {
        $product =  Product::first();
        $data = ['sku' => $product->sku, 'name' => $product->name, 'price' => $product->price];
        $response = $this->call('PATCH', '/api/product/' . $product->id, $data);

        $this->assertEquals(422, $response->status());
    }

    /**
     * Delete product test.
     *
     * @return void
     */
    public function testDeleteProduct()
    {
        $product =  Product::first();
        $response = $this->call('DELETE', '/api/product/' . $product->id);

        $this->assertEquals(200, $response->status());
    }
}
