<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Get all products
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function index(Request $request)
    {
        try {
            $page = max(1, intval($request->query('page', '1')));
            $limit = max(1, intval($request->query('limit', '10')));
            $sortDesc = filter_var($request->query('desc', 'false'), FILTER_VALIDATE_BOOLEAN);
            $sortBy = $request->query('sortBy', 'id');

            // Build the query
            $products = Product::when(
                in_array($sortBy, ['id', 'name', 'sku']),
                function ($q) use ($sortBy, $sortDesc): void {
                    $q->orderBy($sortBy, $sortDesc ? 'desc' : 'asc');
                }
            )->paginate($limit, ['*'], 'page', $page);

            return $this->successResponse($products);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->errorResponse('Internal server error', 500);
        }
    }

    /**
     * Show single product
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function show($product)
    {
        $product = Product::findOrFail($product);
        return $this->successResponse($product);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'sku' => 'required|max:50|unique:products',
            'price' => 'required|numeric|min:1|max:5000'
        ];

        $this->validate($request, $rules);
        $product = Product::create($request->all());
        return $this->successResponse($product);
    }

    /**
     * Update product
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function update(Request $request, $product)
    {
        $rules = [
            'name' => 'required|max:255',
            'sku' => 'required|max:50|unique:products,sku,' . $product,
            'price' => 'required|numeric|min:1|max:5000'
        ];

        $this->validate($request, $rules);
        $product = Product::findOrFail($product);
        $product = $product->fill($request->all());

        if ($product->isClean()) {
            return $this->errorResponse(
                'at least one value must be change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $product->save();
        return $this->successResponse($product);
    }

    /**
     * Delete product
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function destroy($product)
    {

        $product = Product::findOrFail($product);
        $product->delete();
        $message = 'The product was deleted successfully!';
        return $this->successResponse($message);
    }
}
