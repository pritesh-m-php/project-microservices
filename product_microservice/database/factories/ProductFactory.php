<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => $this->faker->isbn10,
            'name' => $this->faker->sentence($nbWords = 4, $variableNbWords = true),
            'price' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 2500)
        ];
    }
}
