<?php

declare(strict_types = 1);

return [
    'products' => [
        'base_uri' => env('PRODUCTS_SERVICE_BASE_URI'),
        'secret' => env('PRODUCTS_SERVICE_SECRET')
    ],
    'users' => [
        'base_uri' => env('USERS_SERVICE_BASE_URI'),
        'secret' => env('USERS_SERVICE_SECRET'),
    ]
];
