<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class GatewayTest extends TestCase
{
    /**
     * User services endpoint test.
     *
     * @return void
     */
    public function testUserServicesEndpoint()
    {
        $this->get('/api/user');
        $this->assertJson(true);
    }

    /**
     * Product services endpoint test.
     *
     * @return void
     */
    public function testProductServicesEndpoint()
    {
        $this->get('/api/user');
        $this->assertJson(true);
    }
}
