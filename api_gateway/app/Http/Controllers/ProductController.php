<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ProductService;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;

class ProductController extends Controller
{

    private $productService;

    /**
     * ProductController constructor.
     *
     * @param \App\Services\ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        try {
            return $this->successResponse($this->productService->fetchProducts());
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with product services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param $product
     *
     * @return mixed
     */
    public function show($product)
    {
        try {
            return $this->successResponse($this->productService->fetchProduct($product));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with product services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        try {
            return $this->successResponse($this->productService->createProduct($request->all()));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with product services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $product
     *
     * @return mixed
     */
    public function update(Request $request, $product)
    {
        try {
            return $this->successResponse($this->productService->updateProduct($product, $request->all()));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with product services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param $product
     *
     * @return mixed
     */
    public function destroy($product)
    {
        try {
            return $this->successResponse($this->productService->deleteProduct($product));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with product services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
