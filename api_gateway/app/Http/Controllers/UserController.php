<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Services\ProductService;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * @var \App\Services\ProductService
     */
    protected $productService;

    /**
     * UserController constructor.
     *
     * @param \App\Services\UserService   $userService
     * @param \App\Services\ProductService $productService
     */
    public function __construct(UserService $userService, ProductService $productService)
    {
        $this->userService = $userService;
        $this->productService = $productService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        try {
            return $this->successResponse($this->userService->fetchUsers());
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with user services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param $user
     *
     * @return mixed
     */
    public function show($user)
    {
        try {
            return $this->successResponse($this->userService->fetchUser($user));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with user services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        try {
            return $this->successResponse($this->userService->createUser($request->all()));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with user services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $user
     *
     * @return mixed
     */
    public function update(Request $request, $user)
    {
        try {
            return $this->successResponse($this->userService->updateUser($user, $request->all()));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with user services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param $user
     *
     * @return mixed
     */
    public function destroy($user)
    {
        try {
            return $this->successResponse($this->userService->deleteUser($user));
        } catch (ConnectException $e) {
            return $this->errorResponse(
                'Not being able to connect with user services',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
