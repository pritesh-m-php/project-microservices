<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller

{
    /**
     * Get all users
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function index(Request $request)
    {
        try {
            // Retrieve parameters from the request
            $page = max(1, intval($request->query('page', '1')));
            $limit = max(1, intval($request->query('limit', '10')));
            $sortDesc = filter_var($request->query('desc', 'false'), FILTER_VALIDATE_BOOLEAN);
            $sortBy = $request->query('sortBy', 'id');
            $emailFilter = $request->query('email', '');

            // Build the query
            $users = User::when($emailFilter, function ($q) use ($emailFilter): void {
                $q->where('email', 'like', '%' . $emailFilter . '%');
            })
                ->when(in_array($sortBy, ['id', 'email']), function ($q) use ($sortBy, $sortDesc): void {
                    $q->orderBy($sortBy, $sortDesc ? 'desc' : 'asc');
                })
                ->paginate($limit, ['*'], 'page', $page);

            return $this->successResponse($users);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->errorResponse('Internal server error', 500);
        }
    }

    /**
     * Show single user
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function show($user)
    {
        $user = User::findOrFail($user);
        return $this->successResponse($user);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ];

        $this->validate($request, $rules);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return $this->successResponse($user);
    }

    /**
     * Update user
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function update(Request $request, $user)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,' . $user,
            'password' => 'confirmed',
        ];

        $this->validate($request, $rules);
        $user = User::findOrFail($user);
        $user = $user->fill($request->all());

        if ($user->isClean()) {
            return $this->errorResponse(
                'at least one value must be change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user->save();
        return $this->successResponse($user);
    }

    /**
     * Delete user
     * @return \Illuminate\Http\JsonResponse The JSON response with the success status, data, and message.
     */
    public function destroy($user)
    {
        $user = User::findOrFail($user);
        $user->delete();
        $message = 'The user was deleted successfully!';
        return $this->successResponse($message);
    }
}
