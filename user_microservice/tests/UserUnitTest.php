<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserUnitTest extends TestCase
{

    public function testUsereModel()
    {

        $faker = \Faker\Factory::create();
        $name = $faker->name();
        $email = $faker->safeEmail();
        $fakePassword = $faker->password();

        $userData = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($fakePassword)
        ];

        $user = User::create($userData);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($name, $user->name);
    }

    public function testValidateUserNotExist()
    {
        $email = 'unittestuser@gmail.com';
        $user =  User::where('email', $email)->first();
        switch ($user) {
                // Check user not exist or not
            case null:
                $this->assertEquals(null, $user);
                break;
            default:
                $this->assertEquals($email, $user->email);
        }
    }
}
