<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTest extends TestCase
{

    /**
     * Create user test.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $faker = \Faker\Factory::create();
        $name = $faker->name();
        $email = $faker->safeEmail();
        $fakePassword = $faker->password();
        $password = Hash::make($fakePassword);
        $user = ['name' => $name, 'email' => $email, 'password' => $password, 'password_confirmation' => $password];

        $response = $this->call('POST', '/api/user', $user);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Get all users test.
     *
     * @return void
     */
    public function testGetUsers()
    {
        $response = $this->call('GET', '/api/user');

        $this->assertEquals(200, $response->status());
    }

    /**
     * Get user by email filter test.
     *
     * @return void
     */
    public function testGetUsersWithFilter()
    {
        $email =  User::first()->email;

        $response = $this->call('GET', '/api/user', ['email' => $email]);
        $this->assertEquals(200, $response->status());
    }

    /**
     * Show user test.
     *
     * @return void
     */
    public function testShowUser()
    {
        $user =  User::first()->id;
        $response = $this->call('GET', '/api/user/' . $user);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Update user with no change test.
     *
     * @return void
     */
    public function testUpdateUserWithNoChange()
    {
        $user =  User::first();

        $data = ['name' => $user->name, 'email' => $user->email];
        $response = $this->call('PATCH', '/api/user/' . $user->id, $data);

        $this->assertEquals(422, $response->status());
    }

    /**
     * Update user test.
     *
     * @return void
     */
    public function testUpdateUser()
    {
        $user =  User::first();

        $faker = \Faker\Factory::create();
        $name = $faker->name();
        $email = $faker->safeEmail();
        $fakePassword = $faker->password();

        $password = Hash::make($fakePassword);

        $data = ['name' => $name, 'email' => $email, 'password' => $password, 'password_confirmation' => $password];
        $response = $this->call('PATCH', '/api/user/' . $user->id, $data);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Delete user test.
     *
     * @return void
     */
    public function testDeleteUser()
    {
        $user =  User::first();

        $response = $this->call('DELETE', '/api/user/' . $user->id);

        $this->assertEquals(200, $response->status());
    }
}
