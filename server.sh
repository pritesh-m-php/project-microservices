#!/bin/bash

PHP_VERSION="8.2"

if ! php$PHP_VERSION -v >/dev/null 2>&1; then
  echo "Please install PHP $PHP_VERSION."
fi

# Function to start PHP development server
start_php_server() {
  local port=$1
  local root_dir=$2
  php$PHP_VERSION -S "localhost:$port" -t "$root_dir" > /dev/null 2>&1 &
}

# Function to stop PHP development server
stop_php_server() {
  local port=$1
  local process_id
  process_id=$(lsof -ti :$port)
  if [ -n "$process_id" ]; then
    kill -9 $process_id
  fi
}


if [ "$1" == "start" ]; then
  # Stop all requred PHP development servers
  stop_php_server 8200
  stop_php_server 8100
  stop_php_server 8000
  # Start PHP development servers
  start_php_server 8200 "user_microservice/public"
  start_php_server 8100 "product_microservice/public"
  start_php_server 8000 "api_gateway/public"
  echo "PHP development servers started for UserServices, ProductService, and APIGateway."
elif [ "$1" == "stop" ]; then
  # Stop PHP development servers
  stop_php_server 8200
  stop_php_server 8100
  stop_php_server 8000
else
  echo "Usage: $0 [start|stop]"
fi
